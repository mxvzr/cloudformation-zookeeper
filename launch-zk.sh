#!/usr/bin/env bash

id=$1

if [[ -z $1 ]]; then
    echo "usage: ./launch-zk.sh <stack_id>"
    exit 1
fi

echo "Creating stack with id '$id'"

aws cloudformation create-stack \
    --template-body file://zookeeper.json \
    --stack-name zk-$id \
    --capabilities CAPABILITY_IAM \
    --parameters \
        ParameterKey=KeyName,ParameterValue=clever \
        ParameterKey=AvailabilityZones,ParameterValue=us-west-1a \
        ParameterKey=InstanceType,ParameterValue=t2.small \
        ParameterKey=ExhibitorS3Bucket,ParameterValue=mesos-exhibitor \
        ParameterKey=ExhibitorS3Region,ParameterValue=us-west-1 \
        ParameterKey=ExhibitorS3Prefix,ParameterValue=me-$id \
        ParameterKey=VpcId,ParameterValue=vpc-c75728ae \
        ParameterKey=Subnets,ParameterValue='subnet-84d130dd' \
        ParameterKey=AdminSecurityGroup,ParameterValue=sg-6e15a00b
